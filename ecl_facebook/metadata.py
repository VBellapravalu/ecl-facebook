__version__ = "1.3.5"
__author__ = "Dan Loewenherz"
__copyright__ = "Copyright 2012, Elm City Labs, LLC"
__maintainer__ = "Dan Loewenherz"
__email__ = "dan@elmcitylabs.com"
__license__ = "Apache 2.0"

